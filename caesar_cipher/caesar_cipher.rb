
def caesar_cipher(string, num)
  result = ""
  special_chars = %w[? ! , . ; : & % # ( ) { } ]
  string.each_char do |char|
    if char.include?(" ") || special_chars.include?(char)
      result += char
    else
      result += caesar_character(char, num)
    end
  end
  result
end


def caesar_character(char, num)
  char_min?(char) ? caesar_cipher_character(char, num, ('a'..'z').to_a) : caesar_cipher_character(char, num, ('A'..'Z').to_a)
end


def caesar_cipher_character(char, num, alp_array)
  min_alp = alp_array
  orig_char_idx = min_alp.index(char)
  orig_char_idx + num < min_alp.length ? min_alp[orig_char_idx + num] : min_alp[orig_char_idx + num - min_alp.length]
end

def char_min?(char)
  ('a'..'z').include?(char)
end
