require 'spec_helper'
require_relative '../caesar_cipher/caesar_cipher'

RSpec.describe 'Caesar Cipher Tests' do

  describe 'check correct character is returned' do

    it 'returns the expected minuscle character' do
      expect(caesar_character("a", 1)).to eq("b")
      expect(caesar_character("a", 2)).to eq("c")
      expect(caesar_character("a", 4)).to eq("e")
      expect(caesar_character("a", 5)).to eq("f")
      expect(caesar_character("x", 1)).to eq("y")
      expect(caesar_character("x", 2)).to eq("z")
      expect(caesar_character("x", 3)).to eq("a")
      expect(caesar_character("x", 4)).to eq("b")
      expect(caesar_character("z", 1)).to eq("a")
      expect(caesar_character("z", 2)).to eq("b")
    end

    it 'returns the expected cap character' do
      expect(caesar_character("A", 1)).to eq("B")
      expect(caesar_character("A", 2)).to eq("C")
      expect(caesar_character("A", 4)).to eq("E")
      expect(caesar_character("A", 5)).to eq("F")
      expect(caesar_character("X", 1)).to eq("Y")
      expect(caesar_character("X", 2)).to eq("Z")
      expect(caesar_character("X", 3)).to eq("A")
      expect(caesar_character("X", 4)).to eq("B")
      expect(caesar_character("Z", 1)).to eq("A")
      expect(caesar_character("Z", 2)).to eq("B")
    end
  end

  describe 'checks if character is min or cap' do

    it 'returns correct true or false value' do
      expect(char_min?("a")).to eq(true)
      expect(char_min?("k")).to eq(true)
      expect(char_min?("z")).to eq(true)
      expect(char_min?("A")).to eq(false)
      expect(char_min?("K")).to eq(false)
      expect(char_min?("Z")).to eq(false)
    end

  end

  describe 'returns whole sentences including spaces' do

    it 'returns the whole sentence without spaces' do
      list = "AbcdefG"
      expect(caesar_cipher(list, 1)).to eq('BcdefgH')
    end

    it 'returns a sentence with spaces'  do
      list = "Abc defG"
      expect(caesar_cipher(list, 1)).to eq('Bcd efgH')
    end

    it 'returns the whole sentence without spaces, but crossing the final z' do
      list = "QrStUvWxYz"
      expect(caesar_cipher(list, 10)).to eq('AbCdEfGhIj')
    end

    it 'returns a sentence with spaces'  do
      list = "Qr StU vWxYz"
      expect(caesar_cipher(list, 10)).to eq('Ab CdE fGhIj')
    end
  end

  describe 'returns the correct string from the assignment' do
    it 'checks the string from the assignment' do
    expect(caesar_cipher("What a string!", 5)).to eq('Bmfy f xywnsl!')
  end
  end
end
